import random


from django.core.management.base import BaseCommand

from faker import Faker

from groups.models import Group

from teachers.models import Teacher


class Command(BaseCommand):
    help = u'Create random group' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Quantity created groups')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        fake = Faker()
        teachers = list(Teacher.objects.all())
        for _ in range(count):
            Group.objects.create(
                group_name=fake.country(),
                course_name=fake.city(),
                date_start_course=fake.date_of_birth(),
                teacher=random.choice(teachers)
                )
