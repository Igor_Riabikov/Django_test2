from core.utils import get_paginator

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy

from django.shortcuts import render, get_object_or_404  # noqa
from django.views.generic import CreateView, UpdateView


from groups.forms import GroupFormCreate, GroupFormList
from groups.models import Group

from students.forms import ViewsStudentsForm


def create_group(request):
    if request.method == "GET":
        form = GroupFormCreate()
    elif request.method == "POST":
        form = GroupFormCreate(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:list"))
    return render(
            request=request,
            template_name='groups-create.html',
            context={'form': form}
        )


def get_groups(request):
    form = GroupFormList()
    if not request.GET:
        groups = Group.objects.all()
    groups = form.get_groups(request)

    return render(
        request=request,
        template_name='groups-list.html',
        context={
            'form': form,
            'groups': get_paginator(groups, request)

            }
        )


def view_group(request, id): # noqa
    try:
        group = Group.objects.get(id=id)
    except Group.DoesNotExist:
        return HttpResponse("Group doesn't exist", status=404)

    if request.method == 'GET':
        form = GroupFormCreate(instance=group)
        st_form = ViewsStudentsForm(request)
        students = st_form.view_students(request=request, group_id=id)
        return render(
            request=request,
            template_name='g-view.html',
            context={
                'form': form,
                'groups': group,
                'students': get_paginator(students, request),

                }
            )

def edit_groups(request, id): # noqa
    try:
        group = Group.objects.get(id=id)
    except Group.DoesNotExist:
        return HttpResponse("Group doesn't exist", status=404)

    if request.method == 'GET':

        form = GroupFormCreate(instance=group)

    elif request.method == 'POST':

        form = GroupFormCreate(
            data=request.POST,
            instance=group
        )

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('groups:list'))

    return render(
        request=request,
        template_name='groups-edit.html',
        context={
            'form': form,
            'group': group,

        }
    )


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupFormCreate
    template_name = 'groups-create.html'
    success_url = reverse_lazy('groups:list')
    context_object_name = 'group'


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupFormCreate
    template_name = 'groups-edit.html'
    success_url = reverse_lazy('groups:list')
    context_object_name = 'group'
    pk_url_kwarg = 'id'


def delete_group(request, id): # noqa
    group = get_object_or_404(Group, id=id)
    group.delete()
    return HttpResponseRedirect(reverse('groups:list'))
