from django.forms import CharField, DateField, DateInput, ModelForm, forms

from groups.models import Group


class GroupFormCreate(ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        widgets = {'date_start_course': DateInput(attrs={'type': 'date'})}


class GroupFormList(forms.Form):
    group_name = CharField(label='Group name', required=False)
    course_name = CharField(label='Course name', required=False)
    date_start_course = DateField(widget=DateInput(attrs={'type': 'date'}), label='Start course date', required=False)

    def get_groups(self, request):

        groups = Group.objects.all()

        fields = [field for field in self.fields]

        for field in fields:
            value = request.GET.get(field)
            if value:
                groups = groups.filter(**{field: value})

        return groups
