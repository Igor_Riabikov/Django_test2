from groups.models import Group


def get_group(request):
    '''
    Views a filtered list teachers from model 'Teacher' from DB
    :param request: request which contains the attribute for filtering select
    :return: Selected teachers list
   '''
    groups = Group.objects.all()

    params = [
        'group_name',
        'course_name',
        'quantity_students',
        'date_start_course'
    ]
    for param in params:
        value = request.GET.get(param)
        if value:
            groups = groups.filter(**{param: value})

    return groups
