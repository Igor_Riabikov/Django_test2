"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin  # noqa
from django.urls import path

from groups.views import GroupCreateView, GroupUpdateView, delete_group, get_groups, view_group

from students.views import hello

app_name = 'groups'

urlpatterns = [
    path('', hello, name="hello"),
    path('create', GroupCreateView.as_view(), name="create"),
    path('list/', get_groups, name="list"),
    path('view/<int:id>', view_group, name="view"),
    path('edit/<int:id>', GroupUpdateView.as_view(), name="edit"),
    path('delete/<int:id>', delete_group, name="delete"),
 ]
