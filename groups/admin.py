from django.contrib import admin # noqa

from groups.models import Group

admin.site.register(Group)
