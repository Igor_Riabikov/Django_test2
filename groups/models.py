import datetime

from django.core.exceptions import ValidationError
from django.db import models

from students.models import Student


class Group(models.Model):
    group_name = models.CharField(max_length=25, null=False, blank=False)
    course_name = models.CharField(max_length=85, null=False, blank=False)
    date_start_course = models.DateField(default=datetime.date.today, blank=False)
    teacher = models.ForeignKey(
        to="teachers.Teacher",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='groups'
    )
    head = models.OneToOneField(
        to='students.Student',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='head_of_group'
    )

    def __str__(self):
        return f'{self.id}, {self.group_name}, {self.course_name}, {self.date_start_course}'

    def save(self, *args, **kwargs):
        self.clean_head()
        super().save(*args, **kwargs)

    def clean_head(self):
        head = self.head
        if head is not None:
            st_group = Student.objects.filter(group_id=self.id)
            if head not in st_group:
                raise ValidationError('not in this group')
            return head
