import random
import uuid

from PIL import Image

from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator


def format_list(lst):
    '''
    Formats the displayed data to HTML
    :param lst: list for formatting
    :return: Formatted data to HTML
    '''
    return '<br>'.join(
        str(elem)
        for elem in lst
    )


def gen_password(length):
    '''
    Generates a password with a given number of characters
    :param length: reports the number of characters
    :return: Generated password
    '''
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_length(request, default=10):
    '''
    Parse attribute 'length' from request
    :param request: parsed request which contains the attribute 'length'
    :return: Parsed attribute 'length'
    '''
    # Анализ введенного параметра длины пароля
    length = request.GET.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def parse_count(count):
    '''
    Parse attribute 'count'
    :param count: count indicates the number of items generated
    :return: Parsed attribute 'count'
    '''
    if count is None:
        raise ValueError("Need param. 'Count='")

    if not count.isnumeric():
        raise ValueError("VALUE ERROR: int")

    count = int(count)

    if not 1 <= count <= 100:
        raise ValueError("RANGE ERROR: [1..100]")

    return count


def generate_uuid():
    return uuid.uuid4().hex


def get_paginator(list_objects, request):
    page = request.GET.get('page')
    paginator = Paginator(list_objects, 6, orphans=2)
    try:
        list_objects = paginator.page(page)
    except PageNotAnInteger:
        list_objects = paginator.page(1)
    except EmptyPage:
        list_objects = paginator.page(paginator.num_pages)
    return list_objects


def resize_image(image, max_size):
    profile_image = Image.open(image)
    width, height = profile_image.size
    if width >= height:
        new_width = max_size
        new_height = int(height * new_width // width)
    else:
        new_height = max_size
        new_width = int(width * new_height // height)
    profile_image_resize = profile_image.resize((new_width, new_height), Image.ANTIALIAS)
    profile_image_resize.save(image.file.name)
    return profile_image_resize
