import datetime

from django.db import models # noqa


class BaseModel(models.Model):
    class Meta:
        abstract = True
    create_time = models.DateTimeField(auto_now_add=True)
    write_time = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.write_time = datetime.datetime.now()
        super().save(*args, **kwargs)


class Person(BaseModel):
    first_name = models.CharField(max_length=64, null=False, blank=False)
    last_name = models.CharField(max_length=64, null=False, blank=True)

    def __str__(self):
        return f'{self.first_name}, {self.last_name}'


# p1 = Person.objects.last()
# p1.last_name = "S makom"
# p1.save()
# print(p1)
