from core.utils import format_list, gen_password, get_paginator, parse_count, parse_length

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView

from django.shortcuts import render, get_object_or_404  # noqa

from students.forms import CreateStudentForm, ViewsStudentsForm
from students.models import Student
from students.utils import get_students_db


def hello(request):
    return HttpResponse("Hello from Django!")


def get_random(request):
    try:
        length = parse_length(request, 10)
        # chars = parse_chars(request, 0)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)
    result = gen_password(length)
    return HttpResponse(result)


def generate_students(request):
    count = request.GET.get("Count")
    try:
        count = parse_count(count)
    except Exception as ex:
        return HttpResponse(str(ex), status=400)
    if Student.gen_students(count) is True:
        return HttpResponse("Student(s) " + str(count) + " added.")


def get_students(request):
    student_list = get_students_db()
    return HttpResponse(format_list(student_list))


def students_list(request):

    students = Student.objects.all()
    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    rating = request.GET.get('rating')
    phone_number = request.GET.get('phone_number')

    if first_name:
        or_names = first_name.split('|')
        or_cond = Q()
        for or_name in or_names:
            or_cond = or_cond | Q(first_name=or_name)
        students = students.filter(or_cond)

    if last_name:
        students = students.filter(last_name=last_name)

    if rating:
        students = students.filter(rating=rating)

    if phone_number:
        students = students.filter(rating=phone_number)

    return render(
        request=request,
        template_name='students-list.html',
        context={
            'students': format_list(students)
        }
    )


def create_student(request):

    if request.method == 'GET':
        form = CreateStudentForm()
    elif request.method == 'POST':
        form = CreateStudentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:list"))
    return render(
        request=request,
        template_name='students-create.html',
        context={'form': form}
    )


def get_student(request):

    if request.method == 'GET':
        form = ViewsStudentsForm()
    elif request.method == 'POST':
        form = ViewsStudentsForm(request.POST)

    students = form.get_students(request)
    return render(
        request=request,
        template_name='students-list.html',
        context={'form': form,
                 'students': get_paginator(students, request)
                 }

            )


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = CreateStudentForm
    template_name = 'students-edit.html'
    success_url = reverse_lazy('students:list')
    context_object_name = 'student'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = CreateStudentForm
    template_name = 'students-create.html'
    success_url = reverse_lazy('students:list')
    context_object_name = 'student'


def delete_student(request, uuid):
    student = get_object_or_404(Student, uuid=uuid)
    student.delete()
    return HttpResponseRedirect(reverse('students:list'))
