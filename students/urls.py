"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin  # noqa
from django.urls import path

from students.views import StudentCreateView, StudentUpdateView, delete_student, \
                            generate_students, get_random, get_student, get_students, hello

app_name = 'students'

urlpatterns = [
    path('', hello, name="hello"),
    path('password/', get_random, name="password"),
    path('generate_students/', generate_students, name="gen_students"),
    path('students/', get_students, name="view_all_students"),
    path('list/', get_student, name="list"),
    path('create/', StudentCreateView.as_view(), name="create"),
    path('edit/<uuid:uuid>', StudentUpdateView.as_view(), name="edit"),
    path('delete/<uuid:uuid>', delete_student, name="delete"),

 ]
