from django import forms
from django.core.exceptions import ValidationError
from django.forms import CharField, DateInput

from students.models import Student


class CreateStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = (
            'first_name',
            'last_name',
            'birthdate',
            'rating',
            'phone_number',
            'group'

        )
        widgets = {'birthdate': DateInput(attrs={'type': 'date'})}

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        if not phone_number.isnumeric():
            raise ValidationError('Please input correct number 380000000000')
        return phone_number


class ViewsStudentsForm(forms.Form):
    first_name = CharField(max_length=64, required=False)
    last_name = CharField(max_length=64, required=False)
    rating = CharField(max_length=3, required=False)
    phone_number = CharField(max_length=12, required=False)

    def get_students(self, request):

        students = Student.objects.select_related('group').all()

        fields = [field for field in self.fields]

        for field in fields:
            value = request.GET.get(field)
            if value:
                students = students.filter(**{field: value})

        return students

    def view_students(self, request, group_id):
        students = Student.objects.select_related('group').filter(group_id=group_id)

        fields = [field for field in self.fields]

        for field in fields:
            value = request.GET.get(field)
            if value:
                students = students.filter(**{field: value})

        return students
