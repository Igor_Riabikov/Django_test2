from students.models import Student


def get_students_db():
    '''
    Views a list students from model 'Student' from DB
    :param: None
    :return: Selected students list
    '''
    student_list = []
    all_data = Student.objects.select_related('group').all()

    for elem in all_data:
        student_list.append([elem.id, elem.first_name, elem.last_name, str(elem.birthdate), elem.phone_number])
    return student_list
