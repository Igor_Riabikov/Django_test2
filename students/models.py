import datetime
import random

from core.utils import generate_uuid

from django.db import models

from faker import Faker

# from groups.models import Group


class Student(models.Model):
    first_name = models.CharField(max_length=64, null=False, blank=False)
    last_name = models.CharField(max_length=64, null=False, blank=False)
    birthdate = models.DateField(null=True, default=datetime.date.today, blank=True)
    rating = models.SmallIntegerField(null=False, default=0, blank=False)
    phone_number = models.CharField(null=False, max_length=12, default="380000000000", blank=False)
    uuid = models.UUIDField(null=False, default=generate_uuid, blank=False, editable=False, unique=True)
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    def full_name(self):
        return f'{self.first_name}, {self.last_name}'

    def age(self):
        return datetime.datetime.now().date().year - self.birthdate.year

    def __str__(self):
        return f'{self.id}, {self.full_name()}, {self.birthdate}, {self.age()}, {self.rating}, {self.phone_number}'

    @staticmethod
    def gen_students(count):
        '''
        Generates random students with a given number to model 'Student' from DB
        :param count: reports the number of elements
        :return: Generated students to model
        '''
        fake = Faker()
        for _ in range(count):
            st = Student.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                birthdate=fake.date_of_birth(),
                rating=random.randint(0, 100),
                phone_number='380'+''.join([str(random.randint(0, 9)) for _ in range(9)])
            )
            st.save()
        return True
