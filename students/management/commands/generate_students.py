import random
import uuid

from django.core.management.base import BaseCommand

from faker import Faker

from groups.models import Group

from students.models import Student


class Command(BaseCommand):
    help = u'Create random student' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Quantity created Students')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        fake = Faker()
        groups = list(Group.objects.all())
        for _ in range(count):
            Student.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                birthdate=fake.date_of_birth(),
                rating=random.randint(0, 100),
                phone_number='380' + ''.join([str(random.randint(0, 9)) for _ in range(9)]),
                uuid=uuid.uuid4().hex,
                group=random.choice(groups)
                )
