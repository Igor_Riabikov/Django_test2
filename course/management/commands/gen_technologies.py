from course.models import Technology

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = u'Create courses' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Quantity created Students')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        technologies = ['PyCharm',
                        'Git',
                        'Github',
                        'Flask',
                        'Django',
                        'Kubernetes',
                        'DockerCompose',
                        'Postgresql',
                        'Json',
                        'Java',
                        'Git',
                        'Rest',
                        'Maven',
                        'IntellijIdea',
                        'Json',
                        'Apache',
                        'NGINX',
                        'PHP',
                        'MySQL',
                        'SOLID',
                        'Xdebug',
                        'Symfony',
                        'Laravel',
                        'Twig',
                        'MVC',
                        'Rest',
                        'Redis',
                        'Memcached',
                        'Composer'
                        ]
        time_at_course = [2, 6,	2, 8, 20, 6, 8,	6, 6, 16, 6, 10, 14, 12, 6,	4,
                          4, 10, 4, 4, 4, 2, 4, 6,	4, 6, 4, 4, 4]
        # for i in range(count):
        #     Technology.objects.create(
        #         name=technologies[i],
        #         time_at_course=time_at_course[i],
        #         )
        for _, tech, hours in zip(range(count), technologies, time_at_course):
            Technology.objects.create(name=tech, time_at_course=hours)
