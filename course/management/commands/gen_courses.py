from course.models import Course

from django.core.management import BaseCommand


class Command(BaseCommand):
    help = u'Create courses' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Quantity created Students')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        courses = ['Python Advanced', 'Java Elementary', 'PHP']
        time_course = 64
        for i in range(count):
            Course.objects.create(
                name=courses[i],
                total_time=time_course
                )
