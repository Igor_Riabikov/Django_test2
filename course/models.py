import datetime

from django.db import models


class Technology(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, default="New Technology")
    time_at_course = models.PositiveSmallIntegerField(default=64, null=False)

    def __str__(self):
        return f'{self.name}'


class Course(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, default="New Course")
    total_time = models.PositiveSmallIntegerField(default=64, null=False)
    date_start = models.DateField(default=datetime.date.today)
    technologies = models.ManyToManyField(
        to=Technology,
        related_name='courses'
    )

    def __str__(self):
        return f'{self.name},{self.date_start},{self.total_time}'
