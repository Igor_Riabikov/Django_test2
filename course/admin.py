from course.models import Course, Technology

from django.contrib import admin


class CourseAdmin(admin.ModelAdmin):
    list_display = ['name', 'date_start', 'total_time']
    fields = ['name', 'date_start', 'total_time', 'technologies']


class TechnologyAdmin(admin.ModelAdmin):
    list_display = ['name', 'time_at_course']
    fields = ['name', 'time_at_course']


admin.site.register(Technology, TechnologyAdmin)
admin.site.register(Course, CourseAdmin)
