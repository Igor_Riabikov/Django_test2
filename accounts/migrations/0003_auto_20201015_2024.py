# Generated by Django 3.1 on 2020-10-15 20:24

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_useractions'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='UserActions',
            new_name='UserAction',
        ),
    ]
