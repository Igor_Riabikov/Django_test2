from accounts.forms import AccountChangeForm, AccountCreateForm, AccountProfileUpdateForm, AccountUpdateForm
from accounts.models import Profile, UserAction

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, "User successfully created")
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)
        try:
            profile = self.request.user.profile
        except Exception as ex:  # noqa
            profile = Profile.objects.create(user=self.request.user)
            profile.save()
        user = form.get_user()
        action = UserAction.USER_ACTION.LOGIN
        logged_user = UserAction.objects.create(user=user, action=action)
        logged_user.save()
        messages.success(self.request, f'User {self.request.user.username} is logged')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            messages.success(request, f'{self.request.user.username} successfully logged out')
        else:
            messages.error(request, f'{self.request.user.username} your error message')
        return super().dispatch(request, *args, **kwargs)


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'profile.html'
    form_class = AccountUpdateForm
    success_url = reverse_lazy('core:index')

    def get_object(self, queryset=None):
        return self.request.user

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )
        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            old_image = Profile.objects.get(user=self.request.user).image
            profile_form.save()
            if old_image != self.request.user.profile.image:
                messages.info(request, 'Changed the image')
                user = self.request.user
                action = UserAction.USER_ACTION.CHANGE_PROFILE_IMAGE
                changed_image = UserAction.objects.create(user=user,
                                                          action=action,
                                                          info=f"{old_image} => {self.request.user.profile.image}"
                                                          )
                changed_image.save()
            messages.success(request, f"{self.request.user.username}'s profile successfully updated")
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )


class AccountPasswordUpdate(PasswordChangeView):
    model = User
    template_name = 'password_change_form.html'
    form_class = AccountChangeForm
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f"{self.request.user.username}'s password is changed")
        return result
