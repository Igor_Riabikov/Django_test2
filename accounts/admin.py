from accounts.models import Profile

from django.contrib import admin


class ProfileAdmin(admin.ModelAdmin):
    field = ('user', 'image')
    list_display = ('user', 'image')


admin.site.register(Profile, ProfileAdmin)
