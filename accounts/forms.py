from accounts.models import Profile

from django.contrib.auth.forms import PasswordChangeForm, UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm


class AccountCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', "first_name", 'last_name']


class AccountUpdateForm(UserChangeForm):
    class Meta:
        model = User
        fields = '__all__'


class AccountProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'interests']


class AccountChangeForm(PasswordChangeForm):
    class Meta:
        model = User
        fields = ['old_password', 'new_password1', 'new_password2']
