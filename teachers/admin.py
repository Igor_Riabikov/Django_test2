from django.contrib import admin # noqa

from groups.models import Group

from teachers.models import Teacher


class GroupTable(admin.TabularInline):
    model = Group
    fields = ['group_name', 'course_name', 'date_start_course', 'teacher']
    readonly_fields = fields
    show_change_link = True
    list_per_page = 10


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'school', 'date_start_work']
    fields = ['first_name', 'last_name', 'school', 'date_start_work']
    inlines = [GroupTable]


admin.site.register(Teacher, TeacherAdmin)
