import datetime

from django.db import models


class Teacher(models.Model):
    first_name = models.CharField(max_length=64, null=False, blank=False)
    last_name = models.CharField(max_length=64, null=False, blank=False)
    age = models.SmallIntegerField(null=True, blank=True)
    date_start_work = models.DateField(null=True, default=datetime.date.today, blank=True)
    school = models.CharField(max_length=64, null=True, blank=True)

    def full_name(self):
        return f'{self.first_name}, {self.last_name}'

    def __str__(self):
        return f'{self.id}, {self.full_name()}, {self.age}, {self.date_start_work}, {self.school}'
