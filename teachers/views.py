from core.utils import get_paginator

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView

from groups.models import Group

from teachers.forms import TeacherFormCreate, TeachersFormList
from teachers.models import Teacher


def get_teachers(request):

    form = TeachersFormList()
    if not request.GET:
        teachers = Teacher.objects.all()
    teachers = form.get_teachers(request)
    return render(
        request=request,
        template_name='teachers-list.html',
        context={
            'form': form,
            'teachers': get_paginator(teachers, request)
        }
        )


def view_teachers(request, id): # noqa
    try:
        teacher = Teacher.objects.get(id=id)
    except Teacher.DoesNotExist:
        return HttpResponse("Group doesn't exist", status=404)

    form = TeacherFormCreate(instance=teacher)
    tc_form = TeachersFormList(request)
    teachers = tc_form.view_teacher(request=request, id=id)
    groups = Group.objects.filter(teacher_id=teacher.id)
    return render(
        request=request,
        template_name='t_view.html',
        context={
            'form': form,
            'teachers': teachers,
            'groups': groups
        }
        )


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherFormCreate
    template_name = 'teachers-create.html'
    success_url = reverse_lazy('teachers:list')
    context_object_name = 'teacher'


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    login_url = "accounts:login"
    model = Teacher
    form_class = TeacherFormCreate
    template_name = 'teachers-edit.html'
    success_url = reverse_lazy('teachers:list')
    context_object_name = 'teacher'
    pk_url_kwarg = 'id'


def delete_teacher(request, id): # noqa
    teacher = get_object_or_404(Teacher, id=id)
    teacher.delete()
    return HttpResponseRedirect(reverse('teachers:list'))
