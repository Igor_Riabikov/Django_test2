from django.core.management.base import BaseCommand

from faker import Faker

from teachers.models import Teacher


class Command(BaseCommand):
    help = u'Create random teacher' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Quantity created teachers')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        fake = Faker()
        for _ in range(count):
            Teacher.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                age=fake.random_int(min=22, max=76, step=1),
                date_start_work=fake.date_between(start_date='-13y', end_date='today'),
                school="Hillel"
                )
