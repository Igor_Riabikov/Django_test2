"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin  # noqa
from django.urls import path

from teachers.views import TeacherCreateView, TeacherUpdateView, delete_teacher, get_teachers, view_teachers

app_name = 'teachers'

urlpatterns = [
     path('list', get_teachers, name="list"),
     path('create', TeacherCreateView.as_view(), name="create"),
     path('edit/<int:id>', TeacherUpdateView.as_view(), name="edit"),
     path('delete/<int:id>', delete_teacher, name="delete"),
     path('view/<int:id>', view_teachers, name="view"),

 ]
