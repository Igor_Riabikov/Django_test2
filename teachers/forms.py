from django.forms import CharField, DateField, DateInput, IntegerField, ModelForm, forms

from teachers.models import Teacher


class TeacherFormCreate(ModelForm):
    class Meta:
        model = Teacher
        fields = (
            'first_name',
            'last_name',
            'age',
            'date_start_work',
            'school'
        )
        widgets = {'date_start_work': DateInput(attrs={'type': 'date'})}


class TeachersFormList(forms.Form):
    first_name = CharField(label='First_name', required=False)
    last_name = CharField(label='Last name', required=False)
    age = IntegerField(label='Age', required=False)
    school = CharField(label='School', required=False)
    date_start_work = DateField(widget=DateInput(attrs={'type': 'date'}), label='Start work date', required=False)

    def get_teachers(self, request):

        teachers = Teacher.objects.all()

        fields = [field for field in self.fields]

        for field in fields:
            value = request.GET.get(field)
            if value:
                teachers = teachers.filter(**{field: value})
        return teachers

    def view_teacher(self, request, id): # noqa
        teachers = Teacher.objects.filter(id=id)

        fields = [field for field in self.fields]

        for field in fields:
            value = request.GET.get(field)
            if value:
                teachers = teachers.filter(**{field: value})

        return teachers
