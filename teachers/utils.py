from teachers.models import Teacher


def get_teacher(request):
    '''
    Views a filtered list teachers from model 'Teacher' from DB
    :param request: request which contains the attribute for filtering select
    :return: Selected teachers list
   '''
    teachers = Teacher.objects.all()

    params = [
        'first_name',
        'last_name',
        'age',
        'date_start_work',
    ]
    for param in params:
        value = request.GET.get(param)
        if value:
            teachers = teachers.filter(**{param: value})

    return teachers
